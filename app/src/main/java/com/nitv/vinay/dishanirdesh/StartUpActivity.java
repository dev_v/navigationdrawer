package com.nitv.vinay.dishanirdesh;

import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;


/**
 * Created by Leviathan on 6/16/2017.
 */

public class StartUpActivity extends AppCompatActivity {

    @Override
    public void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.startup_activity);

        Button button = (Button) findViewById(R.id.button);
        Typeface typeface = Typeface.createFromAsset(getApplicationContext().getAssets(),"fonts/HelveticaNeue.ttf");
        button.setTypeface(typeface);

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(StartUpActivity.this,MainActivity.class);
                startActivity(intent);
            }
        });

    }
}
