package com.nitv.vinay.dishanirdesh;

import android.os.Bundle;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;

import com.nitv.vinay.dishanirdesh.adapter.DrawerCustomAdapter;
import com.nitv.vinay.dishanirdesh.adapter.ListViewAdapter;

import java.util.ArrayList;
import java.util.List;


/**
 * Created by Leviathan on 6/19/2017.
 */

public class MainActivity extends AppCompatActivity {

    String TAG = MainActivity.class.getSimpleName();


    ActionBarDrawerToggle mDrawerToggle;
    Toolbar toolbar;
    DrawerLayout drawerLayout;
    RecyclerView navView;
    LinearLayout container;
    LinearLayoutManager mngr;
    List<DrawerItems> iconList = new ArrayList<>();
    List<DrawerItems> selectedIcon = new ArrayList<>();
    List<ALLItems> allList = new ArrayList<>();

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main_activity);

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        drawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawerLayout.setScrimColor(getResources().getColor(android.R.color.transparent));

        navView = (RecyclerView) findViewById(R.id.navView);
        mngr = new LinearLayoutManager(this);
        navView.setLayoutManager(mngr);

        container = (LinearLayout) findViewById(R.id.container);


        final RelativeLayout search_layout = (RelativeLayout) findViewById(R.id.search_layout);
        Button btn = (Button) findViewById(R.id.button2);
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                search_layout.setVisibility(View.GONE);
            }
        });

        getSupportActionBar().setDisplayHomeAsUpEnabled(false);
        getSupportActionBar().setHomeButtonEnabled(true);

        DrawerCustomAdapter adapter = new DrawerCustomAdapter(this, R.layout.row_items, iconList, selectedIcon);
        navView.setAdapter(adapter);
        navView.addOnItemTouchListener(new RecyclerItemClickListener(getApplicationContext(), new RecyclerItemClickListener.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {

                //Changes the icon of clicked item a/c to its position
                DrawerCustomAdapter.selected_item = position;
                navView.getAdapter().notifyDataSetChanged();

                //If you want to show your drawer even after clicking an item, remove this line:
                drawerLayout.closeDrawer(GravityCompat.START);
            }
        }));

        prepareIcons();
        prepareSelectedIcons();
        setupDrawerToggle();
        setupSearchView();

    }

    private void setupSearchView() {

        String[] videos = {"Hello by Adele","Thunder by Imagine Dragons","Still Dre by Snoop Dogg","Trains by Porcupine Tree","Pyro by Kings of Leon","Yellow by Coldplay","One Dance by Drake","The Next Episode by Dr.Dre","Numb by LinkinPark","Hola by Lola"};

        final ListView list = (ListView) findViewById(R.id.search_list);

        for(int i=0; i<videos.length;i++){
            ALLItems allItems = new ALLItems(videos[i]);
            allList.add(allItems);
        }

        final ListViewAdapter adapter = new ListViewAdapter(this,allList,list);
        list.setAdapter(adapter);
        list.setVisibility(View.GONE);

        SearchView search = (SearchView) findViewById(R.id.search);
        search.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                list.setVisibility(View.VISIBLE);
                adapter.filter(newText);
                return false;
            }
        });
    }

    private void prepareSelectedIcons() {
        DrawerItems selectedItem = new DrawerItems(R.drawable.hover_videos);
        selectedIcon.add(selectedItem);

        selectedItem = new DrawerItems(R.drawable.hover_audio);
        selectedIcon.add(selectedItem);

        selectedItem = new DrawerItems(R.drawable.black_article);
        selectedIcon.add(selectedItem);

        selectedItem = new DrawerItems(R.drawable.black_books);
        selectedIcon.add(selectedItem);

        selectedItem = new DrawerItems(R.drawable.black_events);
        selectedIcon.add(selectedItem);

        selectedItem = new DrawerItems(R.drawable.black_moments);
        selectedIcon.add(selectedItem);

    }

    private void prepareIcons() {

        DrawerItems item = new DrawerItems(R.drawable.videos);
        iconList.add(item);

        item = new DrawerItems(R.drawable.audio);
        iconList.add(item);

        item = new DrawerItems(R.drawable.article);
        iconList.add(item);

        item = new DrawerItems(R.drawable.books);
        iconList.add(item);

        item = new DrawerItems(R.drawable.events);
        iconList.add(item);

        item = new DrawerItems(R.drawable.moments);
        iconList.add(item);

    }

    private void setupDrawerToggle() {
        mDrawerToggle = new ActionBarDrawerToggle(this, drawerLayout, toolbar, R.string.home, R.string.away) {

            //This moves the linear layout accordingly when the drawer is opened/closed
            @Override
            public void onDrawerSlide(View drawerView, float slideOffset) {
                super.onDrawerSlide(drawerView, slideOffset);
                container.setX(slideOffset * navView.getWidth());
            }
        };
        mDrawerToggle.syncState();
        drawerLayout.setDrawerListener(mDrawerToggle);
    }

    @Override
    public void onBackPressed() {
        if (drawerLayout.isDrawerOpen(GravityCompat.START)) {
            drawerLayout.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

}
