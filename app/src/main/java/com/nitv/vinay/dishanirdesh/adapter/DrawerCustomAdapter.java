package com.nitv.vinay.dishanirdesh.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.nitv.vinay.dishanirdesh.DrawerItems;
import com.nitv.vinay.dishanirdesh.R;

import java.util.List;


/**
 * Created by Leviathan on 6/19/2017.
 */

public class DrawerCustomAdapter extends RecyclerView.Adapter<DrawerCustomAdapter.MyViewHolder> {

    String TAG = DrawerCustomAdapter.class.getSimpleName();

    private Context context;
    private int resource;
    private List<DrawerItems> iconList;
    public List<DrawerItems> selectedIcon;
    public static int selected_item = -1;

    public DrawerCustomAdapter(Context context, int resource, List<DrawerItems> iconList, List<DrawerItems> selectedIcon) {
        this.context = context;
        this.resource = resource;
        this.iconList = iconList;
        this.selectedIcon = selectedIcon;
        Log.d(TAG, "DrawerCustomAdapter: " + iconList.size());
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.row_items, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {

        //This is done to set different icons when selected and not selected
        if (position == selected_item) {
            holder.icon.setImageResource(selectedIcon.get(position).getIcon());
        } else {
            holder.icon.setImageResource(iconList.get(position).getIcon());
        }
    }

    @Override
    public int getItemCount() {
        return iconList.size();
    }


    class MyViewHolder extends RecyclerView.ViewHolder {
        ImageView icon;

        MyViewHolder(View view) {
            super(view);
            icon = (ImageView) view.findViewById(R.id.imageView);
        }
    }

}
