package com.nitv.vinay.dishanirdesh.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.nitv.vinay.dishanirdesh.ALLItems;
import com.nitv.vinay.dishanirdesh.R;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

/**
 * Created by Leviathan on 6/21/2017.
 */

public class ListViewAdapter extends BaseAdapter {

    private List<ALLItems> allList;
    private Context context;
    private ArrayList<ALLItems> arrayList;
    ListView list;

    public ListViewAdapter(Context context, List<ALLItems> allList, ListView list) {
        this.allList = allList;
        this.context = context;
        this.list = list;
        this.arrayList = new ArrayList<>();
        this.arrayList.addAll(allList);
    }


    @Override
    public int getCount() {
        return allList.size();
    }

    @Override
    public Object getItem(int i) {
        return allList.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {

        LayoutInflater inflater = LayoutInflater.from(context);
        View rView = inflater.inflate(R.layout.search_list_items,null);
        TextView search_item = (TextView) rView.findViewById(R.id.search_items);
        search_item.setText(allList.get(i).getVideo());
        return rView;
    }

    public void filter(String charText) {
        charText = charText.toLowerCase(Locale.getDefault());
        allList.clear();
        if (charText.length() == 0) {
            allList.addAll(arrayList);
            list.setVisibility(View.GONE);
        }
        else
        {
            for (ALLItems ai : arrayList)
            {
                if (ai.getVideo().toLowerCase(Locale.getDefault()).contains(charText))
                {
                    allList.add(ai);
                }
            }
        }
        notifyDataSetChanged();
    }
}
