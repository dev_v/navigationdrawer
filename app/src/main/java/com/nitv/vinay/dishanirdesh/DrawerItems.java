package com.nitv.vinay.dishanirdesh;

/**
 * Created by Leviathan on 6/19/2017.
 */

public class DrawerItems {

    private int icon;

    DrawerItems(int icon) {
        this.icon = icon;
    }

    public int getIcon() {
        return icon;
    }

    public void setIcon(int icon) {
        this.icon = icon;
    }

}
